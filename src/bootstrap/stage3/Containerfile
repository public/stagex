FROM stagex/stage2 as base
ENV ARCH=x86_64
ENV TARGET=${ARCH}-linux-musl
ENV BUILD=i386-unknown-linux-musl
ENV KCONFIG_NOTIMESTAMP=1
ENV MUSL_VERSION 1.2.4
ENV MUSL_SITE http://musl.libc.org/releases
ENV MUSL_FILE musl-${MUSL_VERSION}.tar.gz
ENV MUSL_HASH 7a35eae33d5372a7c0da1188de798726f68825513b7ae3ebe97aaaa52114f039
ENV MUSL_DIR=${HOME}/build-musl
ENV BUSYBOX_VERSION=1.35.0
ENV BUSYBOX_HASH=faeeb244c35a348a334f4a59e44626ee870fb07b6884d68c10ae8bc19f83a694
ENV BUSYBOX_SITE=https://busybox.net/downloads
ENV BUSYBOX_FILE=busybox-${BUSYBOX_VERSION}.tar.bz2
ENV BUSYBOX_DIR ${HOME}/build-busybox
ENV BINUTILS_VERSION 2.35
ENV BINUTILS_HASH 1b11659fb49e20e18db460d44485f09442c8c56d5df165de9461eb09c8302f85
ENV BINUTILS_SITE https://ftp.gnu.org/gnu/binutils
ENV BINUTILS_FILE binutils-${BINUTILS_VERSION}.tar.xz
ENV BINUTILS_DIR ${HOME}/build-binutils
ENV MAKE_VERSION 4.4
ENV MAKE_HASH 581f4d4e872da74b3941c874215898a7d35802f03732bdccee1d4a7979105d18
ENV MAKE_SITE https://ftp.gnu.org/gnu/make
ENV MAKE_FILE make-${MAKE_VERSION}.tar.gz
ENV MAKE_DIR ${HOME}/build-make
ENV GCC_VERSION 13.1.0
ENV GCC_HASH 61d684f0aa5e76ac6585ad8898a2427aade8979ed5e7f85492286c4dfc13ee86
ENV GCC_SITE https://mirrors.kernel.org/gnu/gcc/gcc-${GCC_VERSION}
ENV GCC_FILE gcc-$GCC_VERSION.tar.xz
ENV GCC_DIR ${HOME}/build-gcc
ENV GMP_VERSION 6.2.1
ENV GMP_HASH eae9326beb4158c386e39a356818031bd28f3124cf915f8c5b1dc4c7a36b4d7c
ENV GMP_SITE https://gcc.gnu.org/pub/gcc/infrastructure/
ENV GMP_FILE gmp-${GMP_VERSION}.tar.bz2
ENV GMP_DIR ${HOME}/build-gmp
ENV MPFR_VERSION 4.1.0
ENV MPFR_HASH feced2d430dd5a97805fa289fed3fc8ff2b094c02d05287fd6133e7f1f0ec926
ENV MPFR_SITE https://gcc.gnu.org/pub/gcc/infrastructure/
ENV MPFR_FILE mpfr-${MPFR_VERSION}.tar.bz2
ENV MPFR_DIR ${HOME}/build-mpfr
ENV MPC_VERSION 1.2.1
ENV MPC_HASH 17503d2c395dfcf106b622dc142683c1199431d095367c6aacba6eec30340459
ENV MPC_SITE https://gcc.gnu.org/pub/gcc/infrastructure/
ENV MPC_FILE mpc-${MPC_VERSION}.tar.gz
ENV MPC_DIR ${HOME}/build-mpc
ENV ISL_VERSION 0.24
ENV ISL_HASH fcf78dd9656c10eb8cf9fbd5f59a0b6b01386205fe1934b3b287a0a1898145c0
ENV ISL_SITE https://gcc.gnu.org/pub/gcc/infrastructure/
ENV ISL_FILE isl-${ISL_VERSION}.tar.bz2
ENV ISL_DIR ${HOME}/build-isl
ENV LINUX_SITE https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x/
ENV LINUX_VERSION 6.6
ENV LINUX_HASH d926a06c63dd8ac7df3f86ee1ffc2ce2a3b81a2d168484e76b5b389aba8e56d0
ENV LINUX_FILE linux-${LINUX_VERSION}.tar.xz
ENV LINUX_DIR ${HOME}/build-linux

FROM base as fetch
WORKDIR /home/user
RUN set -eux; \
    curl -OJ ${LINUX_SITE}/${LINUX_FILE}; \
    echo "${LINUX_HASH}  ${LINUX_FILE}" | sha256sum -c; \
    curl --insecure -OJ ${MUSL_SITE}/${MUSL_FILE}; \
    echo "${MUSL_HASH}  ${MUSL_FILE}" | sha256sum -c; \
    curl --insecure -OJ ${BUSYBOX_SITE}/${BUSYBOX_FILE}; \
    echo "${BUSYBOX_HASH}  ${BUSYBOX_FILE}" | sha256sum -c; \
    curl --insecure -OJ ${BINUTILS_SITE}/${BINUTILS_FILE}; \
    echo "${BINUTILS_HASH}  ${BINUTILS_FILE}" | sha256sum -c; \
    curl --insecure -OJ ${MAKE_SITE}/${MAKE_FILE}; \
    echo "${MAKE_HASH}  ${MAKE_FILE}" | sha256sum -c; \
    curl --insecure -OJ ${GCC_SITE}/${GCC_FILE}; \
    echo "${GCC_HASH}  ${GCC_FILE}" | sha256sum -c; \
    curl --insecure -OJ ${GMP_SITE}/${GMP_FILE}; \
    echo "${GMP_HASH}  ${GMP_FILE}" | sha256sum -c; \
    curl --insecure -OJ ${MPFR_SITE}/${MPFR_FILE}; \
    echo "${MPFR_HASH}  ${MPFR_FILE}" | sha256sum -c; \
    curl --insecure -OJ ${MPC_SITE}/${MPC_FILE}; \
    echo "${MPC_HASH}  ${MPC_FILE}" | sha256sum -c; \
    curl --insecure -OJ ${ISL_SITE}/${ISL_FILE}; \
    echo "${ISL_HASH}  ${ISL_FILE}" | sha256sum -c

FROM fetch as extract
RUN set -eux; \
    tar -xf ${LINUX_FILE}; \
    tar -kxzf ${MUSL_FILE}; \
    tar -kxjf ${BUSYBOX_FILE}; \
    tar -kxf ${BINUTILS_FILE}; \
    tar -kxzf ${MAKE_FILE}; \
    tar -kxf ${GCC_FILE}

FROM extract as build
WORKDIR ${MUSL_DIR}
RUN set -eux; \
    ../musl-${MUSL_VERSION}/configure \
        --prefix=/usr \
        --build=${BUILD} \
        --host=${TARGET}; \
    make
WORKDIR ${BINUTILS_DIR}
RUN set -eux; \
    ../binutils-${BINUTILS_VERSION}/configure \
        --build=${BUILD} \
        --host=${TARGET} \
        --prefix=/usr \
        --bindir=/usr/bin \
        --mandir=/usr/share/man \
        --infodir=/usr/share/info \
        --sysconfdir=/etc \
        --disable-nls \
        --disable-multilib \
        --disable-plugins \
        --disable-gprofng \
        --enable-64-bit-bfd \
        --enable-ld=default \
        --enable-install-libiberty \
        --enable-deterministic-archives; \
    make
WORKDIR ${MAKE_DIR}
RUN set -ex; \
    ../make-${MAKE_VERSION}/configure \
        --build=${BUILD} \
        --host=${TARGET} \
        --prefix=/usr \
        --mandir=/usr/share/man \
        --infodir=/usr/share/info \
        --disable-nls; \
	make
WORKDIR ${GCC_DIR}
RUN set -eux; \
    cp ../*.tar.* ../gcc-${GCC_VERSION}; \
    env -C ${HOME}/gcc-${GCC_VERSION} ./contrib/download_prerequisites; \
    ../gcc-${GCC_VERSION}/configure \
        --build=${BUILD} \
        --host=${TARGET} \
        --target=${TARGET} \
        --prefix=/usr \
        --mandir=/usr/share/man \
        --infodir=/usr/share/info \
        --libdir=/usr/lib \
        --disable-cet \
        --disable-fixed-point \
        --disable-libstdcxx-pch \
        --disable-multilib \
        --disable-libsanitizer \
        --disable-nls \
        --disable-werror \
        --enable-__cxa_atexit \
        --enable-default-pie \
        --enable-default-ssp \
        --enable-languages=c,c++ \
        --enable-link-serialization=2 \
        --enable-linker-build-id; \
    make
WORKDIR ${BUSYBOX_DIR}
RUN set -eux; \
    setConfs=' \
        CONFIG_LAST_SUPPORTED_WCHAR=0 \
        CONFIG_STATIC=y \
    '; \
    unsetConfs=' \
        CONFIG_FEATURE_SYNC_FANCY \
        CONFIG_FEATURE_HAVE_RPC \
        CONFIG_FEATURE_INETD_RPC \
        CONFIG_FEATURE_UTMP \
        CONFIG_FEATURE_WTMP \
    '; \
    make \
        -f ../busybox-${BUSYBOX_VERSION}/Makefile \
        KBUILD_SRC=../busybox-${BUSYBOX_VERSION} \
        CROSS_COMPILE=${TARGET}- \
        defconfig; \
    for conf in $unsetConfs; do \
        sed -i \
            -e "s!^$conf=.*\$!# $conf is not set!" \
            .config; \
    done; \
    for confV in $setConfs; do \
        conf="${confV%=*}"; \
        sed -i \
            -e "s!^$conf=.*\$!$confV!" \
            -e "s!^# $conf is not set\$!$confV!" \
            .config; \
        if ! grep -q "^$confV\$" .config; then \
            echo "$confV" >> .config; \
        fi; \
    done; \
    make oldconfig CROSS_COMPILE=${TARGET}-; \
    for conf in $unsetConfs; do \
        ! grep -q "^$conf=" .config; \
    done; \
    for confV in $setConfs; do \
        grep -q "^$confV\$" .config; \
    done; \
    make CROSS_COMPILE=${TARGET}-
WORKDIR ${HOME}/linux-${LINUX_VERSION}
RUN set -eux; \
    make ARCH=${ARCH} headers; \
    find usr/include -name '.*' -delete; \
    rm usr/include/Makefile; \
    rm usr/include/headers_check.pl; \
    cp -rv usr/include ${LINUX_DIR}

FROM build as install
USER 0:0
RUN set -eux; \
    env -C ${BUSYBOX_DIR} make \
        CROSS_COMPILE=${TARGET}- \
        CONFIG_PREFIX=/rootfs \
        install ; \
    env -C ${MUSL_DIR} make DESTDIR=/rootfs install; \
    env -C ${BINUTILS_DIR} make DESTDIR=/rootfs install; \
    env -C ${MAKE_DIR} make DESTDIR=/rootfs install; \
    env -C ${GCC_DIR} make DESTDIR=/rootfs install; \
    cp -Rv ${LINUX_DIR}/* /rootfs/usr/include/; \
    cd /rootfs/; \
    ln -sT /lib lib64; \
    mkdir -p etc tmp var/tmp home/user; \
    echo "user:x:1000:" > etc/group; \
    echo "user:x:1000:1000::/home/user:/bin/sh" > etc/passwd; \
    chown -R 1000:1000 tmp var/tmp home/user; \
    find /rootfs -exec touch -hcd "@0" "{}" +

FROM scratch as package
COPY --from=install /rootfs /
USER 1000:1000
ENTRYPOINT ["/bin/sh"]
ENV ARCH=x86_64
ENV TARGET=${ARCH}-linux-musl
ENV HOST=${TARGET}
ENV BUILD=${TARGET}
ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
ENV USER=user
ENV HOME=/home/user
ENV TZ=UTC
ENV LANG=C.UTF-8
ENV SOURCE_DATE_EPOCH=1
ENV KCONFIG_NOTIMESTAMP=1
ENV PS1="stage3 $ "
