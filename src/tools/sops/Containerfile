FROM stagex/busybox as base
ENV VERSION=3.8.1
ENV SRC_SITE=https://github.com/getsops/sops/archive/refs/tags
ENV SRC_HASH=5ca70fb4f96797d09012c705a5bb935835896de7bcd063b98d498912b0e645a0
RUN echo ${SRC_SITE}/${VERSION}.tar.gz

FROM base as fetch
COPY --from=stagex/go . /
COPY --from=stagex/ca-certificates . /

RUN set -eux; \
    wget ${SRC_SITE}/v${VERSION}.tar.gz; \
    echo "${SRC_HASH}  v${VERSION}.tar.gz" | sha256sum -c;

FROM fetch as build
RUN tar -xvf v${VERSION}.tar.gz
WORKDIR sops-${VERSION}
ENV PWD=/home/user/sops-${VERSION}
ENV GOPATH=${PWD}/cache/go
ENV GOCACHE=${PWD}/cache/
ENV GOWORK=off
ENV GOPROXY=https://proxy.golang.org,direct
ENV GOSUMDB=sum.golang.org

ENV CGO_ENABLED=0
ENV GOHOSTOS=linux
ENV GOHOSTARCH=amd64
ENV GOFLAGS=-trimpath
RUN mkdir -p ${GOPATH}
RUN go build -o bin/sops ./cmd/sops

from build as install
USER 0:0
RUN mkdir -p /rootfs/usr/bin/
RUN cp bin/sops /rootfs/usr/bin/

FROM scratch as package
COPY --from=install /rootfs/ /
ENTRYPOINT ["/usr/bin/sops"]
CMD ["--version"]
